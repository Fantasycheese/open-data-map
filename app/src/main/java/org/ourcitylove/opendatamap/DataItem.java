package org.ourcitylove.opendatamap;

public class DataItem {
    String sDataTitle;
    String sDataFormat;
    String sFields;
    String sOid;
    String sRidUrl;
    String sRid;
    String sLocationType;
    String sFieldLocation;
    String sFieldTitle;
    String sFieldContent;
    String sIsOk;
    String sCodeType;

    public DataItem(String sDataTitle, String sDataFormat, String sFields, String sOid, String sRidUrl, String sRid, String sLocationType, String sFieldLocation, String sFieldTitle, String sFieldContent, String sIsOk, String sCodeType) {
        this.sDataTitle = sDataTitle;
        this.sDataFormat = sDataFormat;
        this.sFields = sFields;
        this.sOid = sOid;
        this.sRidUrl = sRidUrl;
        this.sRid = sRid;
        this.sLocationType = sLocationType;
        this.sFieldLocation = sFieldLocation;
        this.sFieldTitle = sFieldTitle;
        this.sFieldContent = sFieldContent;
        this.sIsOk = sIsOk;
        this.sCodeType = sCodeType;
    }

    public String getsDataTitle() {
        return sDataTitle;
    }

    public void setsDataTitle(String sDataTitle) {
        this.sDataTitle = sDataTitle;
    }

    public String getsDataFormat() {
        return sDataFormat;
    }

    public void setsDataFormat(String sDataFormat) {
        this.sDataFormat = sDataFormat;
    }

    public String getsFields() {
        return sFields;
    }

    public void setsFields(String sFields) {
        this.sFields = sFields;
    }

    public String getsOid() {
        return sOid;
    }

    public void setsOid(String sOid) {
        this.sOid = sOid;
    }

    public String getsRidUrl() {
        return sRidUrl;
    }

    public void setsRidUrl(String sRidUrl) {
        this.sRidUrl = sRidUrl;
    }

    public String getsLocationType() {
        return sLocationType;
    }

    public void setsLocationType(String sLocationType) {
        this.sLocationType = sLocationType;
    }

    public String getsFieldLocation() {
        return sFieldLocation;
    }

    public void setsFieldLocation(String sFieldLocation) {
        this.sFieldLocation = sFieldLocation;
    }

    public String getsFieldTitle() {
        return sFieldTitle;
    }

    public void setsFieldTitle(String sFieldTitle) {
        this.sFieldTitle = sFieldTitle;
    }

    public String getsFieldContent() {
        return sFieldContent;
    }

    public void setsFieldContent(String sFieldContent) {
        this.sFieldContent = sFieldContent;
    }

    public String getsCodeType() {
        return sCodeType;
    }

    public String getsRid() {
        return sRid;
    }

    public void setsRid(String sRid) {
        this.sRid = sRid;
    }

    public String getsIsOk() {
        return sIsOk;
    }

    public void setsIsOk(String sIsOk) {
        this.sIsOk = sIsOk;
    }

    public void setsCodeType(String sCodeType) {
        this.sCodeType = sCodeType;
    }
}
