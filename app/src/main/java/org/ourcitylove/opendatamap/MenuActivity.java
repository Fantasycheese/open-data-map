package org.ourcitylove.opendatamap;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;


public class MenuActivity extends Activity {
    App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        app = (App) this.getApplication();
        ListView lv = (ListView) findViewById(R.id.list);
        lv.setAdapter(app.adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MenuActivity.this, MapActivity.class);
                intent.putExtra("JSON_ITEM", new Gson().toJson(app.dataList.get(i)));
                startActivity(intent);
            }
        });
    }
}
