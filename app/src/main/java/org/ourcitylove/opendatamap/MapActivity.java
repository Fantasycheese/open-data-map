package org.ourcitylove.opendatamap;


import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapActivity extends Activity {

    private String sTitleFieldName;
    private String sContentFieldName;
    private GoogleMap map;
    private String sUrl;
    private String sLocationType;
    private String sFieldLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        DataItem dataItem = new Gson().fromJson(getIntent().getStringExtra("JSON_ITEM"), DataItem.class);

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(25.0172264,121.506378), 12.0f) );

        sUrl = dataItem.getsRidUrl();
        sLocationType = dataItem.getsLocationType();
        sFieldLocation = dataItem.getsFieldLocation();
        sTitleFieldName = dataItem.getsFieldTitle();
        sContentFieldName = dataItem.getsFieldContent();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(this, sUrl, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONArray rows = response.getJSONObject("result").getJSONArray("results");
                    for (int i = 0; i < rows.length(); i++) {
                        JSONObject row = rows.getJSONObject(i);
                        LatLng latLng = null;
                        switch (sLocationType) {
                            case "LL":
                                String sLatFieldName = sFieldLocation.split(",")[0];
                                String sLongFieldName = sFieldLocation.split(",")[1];
                                latLng = new LatLng(Double.valueOf(row.getString(sLatFieldName)),
                                        Double.valueOf(row.getString(sLongFieldName)));
                                break;
                            case "ADDR":
                                Geocoder geoCoder = new Geocoder(MapActivity.this, Locale.TAIWAN);
                                try {
                                    String sAddress = row.getString(sFieldLocation);
                                    List<Address> addr = geoCoder.getFromLocationName(sAddress, 1);
                                    latLng = new LatLng(addr.get(0).getLatitude(),
                                            addr.get(0).getLongitude());

                                } catch (IOException e) { e.printStackTrace(); }
                                break;
                            default: latLng = new LatLng(0,0);
                        }
                        String sTitle = row.getString(sTitleFieldName);
                        String sContent = row.getString(sContentFieldName);
                        map.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title(sTitle).snippet(sContent));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }
}
