package org.ourcitylove.opendatamap;

import android.content.Context;
import android.graphics.Color;
import android.widget.ListView;

import com.james.views.FreeLayout;
import com.james.views.FreeTextView;

public class DataLayout extends FreeLayout {
    public FreeTextView ftvDataTitle;
    public FreeTextView ftvDataFormat;
    public FreeTextView ftvFields;
    public DataLayout(Context context) {
        super(context);
        this.setPicSize(640, 1136, TO_WIDTH);
        this.setLayoutParams(new ListView.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        ftvDataTitle = (FreeTextView) this.addFreeView(new FreeTextView(context), 0, 0, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//        ftvDataFormat = (FreeTextView) this.addFreeView(new FreeTextView(context), 0, 50, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//        ftvFields = (FreeTextView) this.addFreeView(new FreeTextView(context), 0, 100, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        ftvDataTitle.setTextColor(Color.BLACK);
        ftvDataTitle.setTextSizeFitSp(40);
//        ftvDataFormat.setTextColor(Color.BLACK);
//        ftvFields.setTextColor(Color.BLACK);
    }
}
