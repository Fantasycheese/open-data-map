package org.ourcitylove.opendatamap;

import android.content.Context;
import android.view.View;

import com.james.views.FreeAdapter;

import java.util.ArrayList;

/**
 * Created by Joshua on 2015/5/23.
 */
public class MenuAdapter extends FreeAdapter<DataItem, View> {
    private final Context mContext;
    private final ArrayList<DataItem> itemList;

    public MenuAdapter(Context context, ArrayList<DataItem> arrayList) {
        super(context, arrayList);
        mContext = context;
        this.itemList = arrayList;
    }

    @Override
    public View initView(int i) {
        return new DataLayout(mContext);
    }

    @Override
    public void setView(int i, View view) {
        DataItem item = itemList.get(i);
        DataLayout layout = (DataLayout)view;
        layout.ftvDataTitle.setText(item.getsDataTitle());
//        layout.ftvDataFormat.setText(item.getsDataFormat());
//        layout.ftvFields.setText(item.getsFields());
    }
}
