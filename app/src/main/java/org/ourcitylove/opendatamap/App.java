package org.ourcitylove.opendatamap;

import android.app.Application;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class App extends Application {
    ArrayList<DataItem> dataList;
    MenuAdapter adapter;

    @Override
    public void onCreate() {
        super.onCreate();
        AsyncHttpClient client = new AsyncHttpClient();
        String sUrl = "https://spreadsheets.google.com/feeds/list/1QoV7mbBcUFSCfhvO06E6TQkrN3LEK6n58Sf_cV_7Fiw/o1ldluz/public/values?alt=json";
        dataList = new ArrayList<>();
        adapter = new MenuAdapter(this, dataList);
        client.get(this, sUrl, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONArray rows = response.getJSONObject("feed").getJSONArray("entry");
                    for (int i = 0; i < rows.length(); i++) {
                        JSONObject row = rows.getJSONObject(i);
                        dataList.add(new DataItem(
                                row.getJSONObject("gsx$datatitle").getString("$t"),
                                row.getJSONObject("gsx$dataformat").getString("$t"),
                                row.getJSONObject("gsx$fields").getString("$t"),
                                row.getJSONObject("gsx$oid").getString("$t"),
                                row.getJSONObject("gsx$ridurl").getString("$t"),
                                row.getJSONObject("gsx$rid").getString("$t"),
                                row.getJSONObject("gsx$ltype").getString("$t"),
                                row.getJSONObject("gsx$fl").getString("$t"),
                                row.getJSONObject("gsx$ft").getString("$t"),
                                row.getJSONObject("gsx$fn").getString("$t"),
                                row.getJSONObject("gsx$isok").getString("$t"),
                                row.getJSONObject("gsx$codetype").getString("$t")
                        ));
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });

    }
}
